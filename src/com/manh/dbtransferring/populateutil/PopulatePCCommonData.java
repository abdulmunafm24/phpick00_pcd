package com.manh.dbtransferring.populateutil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.manh.dbtransferring.data.DBPropertiesData;
import com.manh.dbtransferring.util.TableUtil;

public class PopulatePCCommonData {

	final static Logger logger = Logger.getLogger(PopulatePCCommonData.class);

	static TableUtil tableUtil = new TableUtil();

	public static void populateInserSQLString() {
		StringBuffer toColumns = new StringBuffer("");
		for (Map.Entry<String, String> entry : DBPropertiesData.columnNames_TypesMap.entrySet()) {
			toColumns.append(entry.getKey() + ",");
		}
		toColumns = tableUtil.removeLastComma(toColumns);
		StringBuffer questionMarks = new StringBuffer("");
		for (int i = 1; i <= DBPropertiesData.columnNames_TypesMap.size(); i++) {
			questionMarks.append("?,");
		}
		questionMarks = tableUtil.removeLastComma(questionMarks);
		DBPropertiesData.inserSQLString = "insert into " + DBPropertiesData.toTable + "(" + toColumns + ") values(" + questionMarks + ")";
		logger.info("In populateInserSQLString DBPropertiesData.inserSQLString: " + DBPropertiesData.inserSQLString);
	}

	public static void populateSelectFromAs400SQLString() {
		StringBuffer fromColumns = new StringBuffer();
		for (Map.Entry<String, String> columnName_Type : DBPropertiesData.columnNames_TypesMap.entrySet()) {
			fromColumns.append("L." + columnName_Type.getKey() + ",");
		}
		fromColumns = tableUtil.removeLastComma(fromColumns);
		DBPropertiesData.selectFromAs400SQLString = "select " + fromColumns + " from " + DBPropertiesData.propertiesMap.get("as400_schemaName") + "." + DBPropertiesData.fromTable + "  L where rrn(L) between " + " ? and ?";
		logger.info("In populateSelectFromAs400SQLString: " + DBPropertiesData.selectFromAs400SQLString);
	}

	/**
	 * 
	 * @param tableName
	 * @return column Names and their types from Oracle Table
	 */
	public static void populateColumnNamesAndTypesFromOracleDB(Connection oracleConnection) {
		String tableName = DBPropertiesData.toTable;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		logger.info("In populateColumnNamesAndTypesFromOracleDB tableName" + tableName);
		try {
			pstmt = oracleConnection.prepareStatement("select * from " + tableName);

			rs = pstmt.executeQuery();
			ResultSetMetaData rsMdata = rs.getMetaData();
			for (int index = 1; index <= rsMdata.getColumnCount(); index++) {
				String columnName = rsMdata.getColumnName(index);
				String columnType = rsMdata.getColumnTypeName(index);
				if (columnType != null && "NUMBER".equals(columnType)) {
					if (rsMdata.getScale(index) != 0) {
						columnType = "DECIMAL_NUMBER";
					}
				}
				DBPropertiesData.columnNames_TypesMap.put(columnName, columnType);
			}
			logger.info("getColumnNames Column Map : " + DBPropertiesData.columnNames_TypesMap);
			logger.info("getColumnNames No of Columns : " + DBPropertiesData.columnNames_TypesMap.size());

		} catch (Exception e) {
			logger.error("Exception Occured !", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (oracleConnection != null) {
					oracleConnection.close();
					// Thread.sleep(1000);
				}

			} catch (Exception e) {
				logger.error("Exception Occured !", e);
			}
		}
	}
}
