package com.manh.dbtransferring.data;

import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class DBPropertiesData {

	public static Properties prop = new Properties();
	public final static String currentDir = System.getProperty("user.dir");
	public static String inserSQLString;
	// public static boolean inserSQLStringPopulated;

	public static String selectFromAs400SQLString;
	// -public static boolean selectFromAs400SQLStringPopulated;

	public static Map<String, String> columnNames_TypesMap = new LinkedHashMap<String, String>();;
	public static String toTable;
	public static String fromTable;

	// THese values are pushed form LoadProperties.java
	public static InputStream input;
	// public static String dbPropertiesPath;
	public static String logFilePath;
	public static String osType;

	// PropertiesMap
	public static Map<String, String> propertiesMap = new HashMap<String, String>();

}
