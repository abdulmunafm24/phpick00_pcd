package com.manh.dbtransferring.data;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

public class ProducerConsumerData {

	final static Logger logger = Logger.getLogger(ProducerConsumerData.class);

	public static long tillNowCommited;

	public static Map<String, String> startingEndingRangesMap = new TreeMap<String, String>();

	public static long totalNoOfRecordsInFromTable;

	public synchronized void pushStartingEndingRanges(long startingRange, long endingRange) {
		logger.info("In pushStartingEndingRanges: startingEndingRangesMap.size():" + startingEndingRangesMap.size());
		while (startingEndingRangesMap.size() > 0) {// Integer.parseInt(DBPropertiesData.propertiesMap.get("maxNoOfThreads"))
			try {
				wait();
			} catch (InterruptedException e) {
				logger.error("Exception: ", e);
			}
		}
		if (totalNoOfRecordsInFromTable == endingRange)
			startingEndingRangesMap.put(startingRange + "_" + endingRange, "");
		else
			startingEndingRangesMap.put(startingRange + "_" + endingRange, "");
		// ("startingEndingRangesMap :" + startingEndingRangesMap);
		logger.info("Notifying Threads startingEndingRangesMap :" + startingEndingRangesMap);
		notifyAll();
		// try {
		// //Thread.sleep(1000*30);
		// } catch (InterruptedException e) {
		// logger.error("Exception: ", e);
		// }
	}

	public synchronized String getStartingEndingRanges() {
		String dataRange = "";
		logger.info("In  getStartingEndingRanges startingEndingRangesMap.size():" + startingEndingRangesMap.size());
		// return "SHUTDOWN" if data is over
		if (startingEndingRangesMap.size() <= 0 && ShutdownThreadData.shutdownThread) {
			return "SHUTDOWN";
		}
		while (startingEndingRangesMap.size() <= 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				logger.error("Exception: ", e);
			}
		}

		logger.info(" startingEndingRangesMap.size():" + startingEndingRangesMap.size());

		// ("startingEndingRangesMap.size()=" + startingEndingRangesMap.size());
		// = startingEndingRangesMap.entrySet().iterator().next();
		for (Map.Entry<String, String> entry : startingEndingRangesMap.entrySet()) {
			dataRange = entry.getKey();
			// ("In For: dataRange:"+dataRange);
			startingEndingRangesMap.remove(dataRange);
			break;
		}

		// startingEndingRangesMap.remove(dataRange);
		logger.info("Returning dataRange: " + dataRange + " startingEndingRangesMap.size():" + startingEndingRangesMap.size());
		// ("Returning dataRange: " + dataRange + " startingEndingRangesMap.size():" + startingEndingRangesMap.size());
		logger.info("Notifying Threads startingEndingRangesMap :" + startingEndingRangesMap + " dataRange:" + dataRange);
		notifyAll();
		return dataRange;
	}

	public synchronized void notifyAllThreads() {
		if (startingEndingRangesMap.size() <= 0) {
			notifyAll();
		}
	}

	public static synchronized void updateTillNowCommited(long tmpTillNowCommited) {
		tillNowCommited += tmpTillNowCommited;
	}
}