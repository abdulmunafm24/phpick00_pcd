package com.manh.dbtransferring.main;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.manh.dbtransferring.data.DBPropertiesData;
import com.manh.dbtransferring.data.ProducerConsumerData;
import com.manh.dbtransferring.data.ShutdownThreadData;
import com.manh.dbtransferring.populateutil.PopulatePCCommonData;
import com.manh.dbtransferring.proputil.LoadProperties;
import com.manh.dbtransferring.util.CustomAS400ConnectionPool;
import com.manh.dbtransferring.util.OracleConnectionPool;
import com.manh.dbtransferring.util.TableUtil;

public class PHPICK00Producer {

	final static Logger logger = Logger.getLogger(PHPICK00Producer.class);

	public static void main(String[] args) throws Exception {
		TableUtil tableUtil = new TableUtil();
		// //Load the properties
		LoadProperties.createDBPropFilePath_LogFilePath();
		LoadProperties.loadAllProperties();
		// Populate all required values in DBPropertiesData
		DBPropertiesData.toTable = "PHPICK00_PCD";
		DBPropertiesData.fromTable = "PHPICK00";

		OracleConnectionPool jdbcObj = new OracleConnectionPool();
		DataSource oracleDataSource = jdbcObj.setUpPool();

		CustomAS400ConnectionPool as400jdbcObj = new CustomAS400ConnectionPool();
		DataSource as400DataSource = as400jdbcObj.setUpPool();

		try {
			if (oracleDataSource.getConnection() == null) {
				// ("\n\n****** Program is not able to connect to Oracle DB ******");
				// ("****** Kindly Check DB Properties and Logs******\n\n");
				logger.info("Oracle Con NULL");
				System.exit(0);
			}
			if (as400DataSource.getConnection() == null) {
				// ("\n\n****** Program is not able to connect to AS400 DB ******");
				// ("****** Kindly Check DB Properties and Logs******\n\n");
				logger.info("as400Con NULL");
				System.exit(0);
			}

			as400DataSource.getConnection().close();
			Thread.sleep(1000);
			oracleDataSource.getConnection().close();
			Thread.sleep(1000);
		} catch (Exception e) {
			logger.error(e);
			return;
		}

		PopulatePCCommonData.populateColumnNamesAndTypesFromOracleDB(oracleDataSource.getConnection());
		PopulatePCCommonData.populateInserSQLString();
		PopulatePCCommonData.populateSelectFromAs400SQLString();

		//

		// //validate the connection detail
		//
		//
		long startTime = System.currentTimeMillis();

		long totalNoOfRecordsInFromTable = tableUtil.getNoOfRowsInTable(DBPropertiesData.propertiesMap.get("as400_schemaName") + "." +DBPropertiesData.fromTable, as400DataSource.getConnection());
		ProducerConsumerData.totalNoOfRecordsInFromTable = totalNoOfRecordsInFromTable;
		logger.info("Total No of Rows in AS400 Table " + totalNoOfRecordsInFromTable);
		long totalNoOfRowsInToTable = tableUtil.getNoOfRowsInTable(DBPropertiesData.toTable, oracleDataSource.getConnection());
		logger.info("Total No of Rows in Oracle To Table " + totalNoOfRowsInToTable);
		int maxNoOfRecordsPerThread = Integer.parseInt(DBPropertiesData.propertiesMap.get("maxNoOfRecordsPerThread"));
		int maxNoOfThreads = Integer.parseInt(DBPropertiesData.propertiesMap.get("maxNoOfThreads"));
		logger.info("****** Log File Created At : " + DBPropertiesData.logFilePath + " ******");
		logger.info("****** Starting Copying Data From " + DBPropertiesData.fromTable + " ******");
		logger.info("****** AS400 schema : " + DBPropertiesData.propertiesMap.get("as400_schemaName"));
		logger.info("****** Total No of Rows In " + DBPropertiesData.propertiesMap.get("as400_schemaName") + "." + DBPropertiesData.fromTable + " : " + totalNoOfRecordsInFromTable);
		logger.info("****** maxNoOfRecordsPerThread: " + maxNoOfRecordsPerThread + " maxNoOfThreads:" + maxNoOfThreads);
		logger.info("****** Before Insert : No of Rows In Oracle Table " + DBPropertiesData.toTable + ": " + totalNoOfRowsInToTable);
		logger.info("****** Program Displays Count Once Any Thread Completes Commiting " + maxNoOfRecordsPerThread + " Records.");

		// Common Data Object
		ProducerConsumerData producerConsumerData = new ProducerConsumerData();
		// Connection Pool Object shoudl be sahred accross threads so that everyone will be accessing same pool

		// Create Consumers
		ConsumerThread consumer = null;

		// Consumers Array
		Set<ConsumerThread> consumers = Collections.newSetFromMap(new ConcurrentHashMap<ConsumerThread, Boolean>());

		// Creating maxNoOfThreads consumer
		for (int i = 1; i <= maxNoOfThreads; i++) {
			consumer = new ConsumerThread(producerConsumerData, oracleDataSource, as400DataSource);// null,null);//
			consumer.setName("Thread_" + i);
			consumers.add(consumer);
			consumer.start();
		}
		// ("Created Threads: " + consumers);
		// (" Going to Sleep");
		logger.info("Created Threads: " + consumers + " Going to Sleep");
		Thread.sleep(1000 * 15);
		logger.info("Completed Sleeping");
		// Push the Data as sets
		long startingRange = -1, endingRange = -1;

		for (; endingRange < totalNoOfRecordsInFromTable;) {
			// logger.info("Pusing startingRange:" + startingRange + " endingRange:" + endingRange);

			if (startingRange == -1) {
				startingRange = 1;
				endingRange = maxNoOfRecordsPerThread;
			} else {
				startingRange = endingRange + 1;
				if (endingRange + maxNoOfRecordsPerThread < totalNoOfRecordsInFromTable)
					endingRange = endingRange + maxNoOfRecordsPerThread;
				else
					endingRange = totalNoOfRecordsInFromTable;
			}
			// ("Pusing startingRange:" + startingRange + " endingRange:" + endingRange);
			logger.info("Pusing startingRange:" + startingRange + " endingRange:" + endingRange);

			producerConsumerData.pushStartingEndingRanges(startingRange, endingRange);
		}

		// ("Completed Pusing Data");
		logger.info("Completed Pusing Data");
		ShutdownThreadData.shutdownThread = true;

		// ("Waiting for threads to exit");
		logger.info("Waiting for threads to exit");

		for (; true;) {
			for (ConsumerThread tmpConsumer : consumers) {
				if (!tmpConsumer.isAlive()) {
					consumers.remove(tmpConsumer);
				}
				producerConsumerData.notifyAllThreads();
			}
			if (consumers.isEmpty())
				break;
		}
		logger.info("** consumers.size();" + consumers.size());
		logger.info("** Total No of Rows in AS400 Table " + totalNoOfRecordsInFromTable);
		totalNoOfRowsInToTable = tableUtil.getNoOfRowsInTable(DBPropertiesData.toTable, oracleDataSource.getConnection());
		logger.info("Total No of Rows in Oracle To Table " + totalNoOfRowsInToTable);
		logger.info("** Total Number Of Records In AS400 table and Records Inserted Into Oracle Table May Be Not Equal. Beacuse Sometimes We Found Only 30 Records Between The Range of 1000 to 10100");
		logger.info("****** Time taken : " + (System.currentTimeMillis() - startTime) / 1000 + " Seconds");
		// ("****** Time taken : " + (System.currentTimeMillis() - startTime) / 1000 + " Seconds");
		// ("Exiting");
		logger.info("Done. Bye. End of the Program. All threads shutdown.");
	}
}
