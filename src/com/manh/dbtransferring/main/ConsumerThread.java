package com.manh.dbtransferring.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.manh.dbtransferring.data.DBPropertiesData;
import com.manh.dbtransferring.data.ProducerConsumerData;
import com.manh.dbtransferring.data.ShutdownThreadData;
import com.manh.dbtransferring.util.TableUtil;

public class ConsumerThread extends Thread {

	final static Logger logger = Logger.getLogger(ConsumerThread.class);

	ProducerConsumerData producerConsumerData;
	DataSource oracleDataSource;
	DataSource customAS400DataSource;
	TableUtil tableUtil = new TableUtil();

	// static long actualRecordsInThisRange = 0;

	public ConsumerThread(ProducerConsumerData producerConsumerData, DataSource oracleDataSource, DataSource customAS400DataSource) {
		super();
		this.producerConsumerData = producerConsumerData;
		this.oracleDataSource = oracleDataSource;
		this.customAS400DataSource = customAS400DataSource;
	}

	@Override
	public void run() {
		// This thread never dies
		String dataRange = "";
		logger.info("In run(): Thread: " + this.getName());
		while (true) {
			logger.info(this.getName() + " In run(): ShutdownThreadData.shutdownThread:" + ShutdownThreadData.shutdownThread + " ProducerConsumerData.startingEndingRangesMap.size():" + ProducerConsumerData.startingEndingRangesMap.size());
			if (!ShutdownThreadData.shutdownThread) {
				dataRange = producerConsumerData.getStartingEndingRanges();
				if ("SHUTDOWN".equals(dataRange))
					break;
			} else if (ProducerConsumerData.startingEndingRangesMap.size() > 0) {
				dataRange = producerConsumerData.getStartingEndingRanges();
				if ("SHUTDOWN".equals(dataRange))
					break;
			} else
				break;

			logger.info("Data Range: dataRange" + dataRange);
			String threadName = this.getName();
			int index = threadName.indexOf("DataRange");
			if (index != -1)
				this.setName(threadName.substring(0, index) + "DataRange:" + dataRange);
			else
				this.setName(threadName + " DataRange:" + dataRange);

			String[] range = dataRange.split("_");
			long startingRange = Long.parseLong(range[0]), endingRange = Long.parseLong(range[1]);

			String[] resultArr = processData(startingRange, endingRange);
			logger.info(this.getName() + " Result From processData success:" + resultArr[0] + " , "+resultArr[1]);
			if ("SUCCESS".equals(resultArr[0])) {
				ProducerConsumerData.updateTillNowCommited(Integer.parseInt(resultArr[1]));
				// ("** Till Now Commited:" + ProducerConsumerData.tillNowCommited);
				logger.info(this.getName() + " ** Till Now Commited Data:" + ProducerConsumerData.tillNowCommited);
			} else {
				logger.info("Batch Failed" + dataRange);
			}
		}
		logger.info("Exiting Thread: " + this.getName());
	}

	private String[] processData(long startingRange, long endingRange) {
		logger.info(this.getName() + " In processData:");
		// String[] range = dataRange.split("_");
		// long startingRange = Long.parseLong(range[0]), endingRange = Long.parseLong(range[1]);
		logger.info(this.getName() + " In processData: startingRange:" + startingRange + " endingRange:" + endingRange);
		long actualRecordsInThisRange = 0;
		String[] resultArr = { "SUCCESS", "" };

		Connection oracleConnection = null, as400Connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			oracleConnection = oracleDataSource.getConnection();
			as400Connection = customAS400DataSource.getConnection();

			// Get the records from as400
			resultSet = tableUtil.getRecordsFromAS400ForTheGivenRange(as400Connection, startingRange, endingRange);
			resultSet.setFetchSize(100);
			oracleConnection.setAutoCommit(false);
			preparedStatement = oracleConnection.prepareStatement(DBPropertiesData.inserSQLString);
			long tmpEndingRange = startingRange;
			while (resultSet.next()) {
				++actualRecordsInThisRange;
				int j = 1;
				for (Map.Entry<String, String> entry : DBPropertiesData.columnNames_TypesMap.entrySet()) {

					if (entry.getValue().equals("CHAR")) {
						preparedStatement.setString(j++, resultSet.getString(entry.getKey()));
					} else if (entry.getValue().equals("NUMBER")) {
						preparedStatement.setBigDecimal(j++, resultSet.getBigDecimal(entry.getKey()));
					} else if (entry.getValue().equals("VARCHAR2")) {
						preparedStatement.setString(j++, resultSet.getString(entry.getKey()));
					} else if ("DECIMAL_NUMBER".equals(entry.getValue())) {
						preparedStatement.setDouble(j++, resultSet.getDouble(entry.getKey()));
					}
				}
				preparedStatement.addBatch();
			}
			int[] resultOfExecuteBatch = preparedStatement.executeBatch();
			String resultStr = "";
			for (int i : resultOfExecuteBatch) {
				resultStr += " " + i;
			}
			oracleConnection.commit();
			logger.info(Thread.currentThread().getName() + " Commited Data Between The Batch : startingRange: " + startingRange + " endingRange: " + endingRange + " actualRecordsInThisRange:" + actualRecordsInThisRange);
			logger.info(" resultOfExecuteBatch.length:" + resultOfExecuteBatch.length + " resultStr:" + resultStr);
			// Thread.sleep(1000 * 15);
		} catch (Exception e) {
			actualRecordsInThisRange = -1;
			resultArr[0] = "FAILED";
			logger.error("Exception", e);
			try {
				oracleConnection.rollback();
			} catch (SQLException e1) {
				logger.error("Exception", e);
			}
			logger.error("Exception", e);
		} finally {
			try {
				// Closing ResultSet Object
				if (resultSet != null) {
					resultSet.close();
				}
				// Closing PreparedStatement Object
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				// Closing Connection Object
				if (oracleConnection != null) {
					oracleConnection.close();
				}
				if (as400Connection != null) {
					as400Connection.close();
				}
			} catch (Exception sqlException) {
				logger.error("Exception Occured !", sqlException);
			}
		}

		// oracleConnectionPool.printDbStatus();
		resultArr[1] = actualRecordsInThisRange + "";
		return resultArr;
	}
}
