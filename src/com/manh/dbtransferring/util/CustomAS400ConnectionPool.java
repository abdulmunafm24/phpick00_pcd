package com.manh.dbtransferring.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.manh.dbtransferring.data.DBPropertiesData;

public class CustomAS400ConnectionPool {
	
	final static Logger logger = Logger.getLogger(CustomAS400ConnectionPool.class);
	
    // JDBC Driver Name & Database URL
    static final String JDBC_DRIVER = "com.ibm.as400.access.AS400JDBCDriver"; //"oracle.jdbc.driver.OracleDriver";  
    static final String JDBC_DB_URL = DBPropertiesData.propertiesMap.get("as400_dbUrl");//"jdbc:oracle:thin:@rockwell.us.manh.com:1523:CSO12R1A";

    // JDBC Database Credentials
    static final String JDBC_USER = DBPropertiesData.propertiesMap.get("as400_dbUser");//"HBAY2016WMSTEST";
    static final String JDBC_PASS = DBPropertiesData.propertiesMap.get("as400_dbPassword");//"HBAY2016WMSTEST";

    private static GenericObjectPool gPool = null;
    
    

    @SuppressWarnings("unused")
    public DataSource setUpPool() throws Exception {
        Class.forName(JDBC_DRIVER);
        logger.info("JDBC_DB_URL:" + JDBC_DB_URL + " JDBC_USER:" + JDBC_USER + " JDBC_PASS:" + JDBC_PASS);
        // Creates an Instance of GenericObjectPool That Holds Our Pool of Connections Object!
        gPool = new GenericObjectPool();
        gPool.setMaxActive(5);

        // Creates a ConnectionFactory Object Which Will Be Use by the Pool to Create the Connection Object!
        ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);

        // Creates a PoolableConnectionFactory That Will Wraps the Connection Object Created by the ConnectionFactory to Add Object Pooling Functionality!
        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
        return new PoolingDataSource(gPool);
    }

    public GenericObjectPool getConnectionPool() {
        return gPool;
    }

    // This Method Is Used To Print The Connection Pool Status
    public void printDbStatus() {
        //("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }

    public static void main(String ag[]){
    	for(int i=1;i<5;i++){
    		main22(ag);
    	}
    }
    public static void main22(String[] args) {
        ResultSet rsObj = null;
        Connection connObj = null;
        PreparedStatement pstmtObj = null;
        OracleConnectionPool jdbcObj = new OracleConnectionPool();
        try {   
            DataSource dataSource = jdbcObj.setUpPool();
            jdbcObj.printDbStatus();

            // Performing Database Operation!
            //("\n=====Making A New Connection Object For Db Transaction=====\n");
            connObj = dataSource.getConnection();
            jdbcObj.printDbStatus(); 

            pstmtObj = connObj.prepareStatement("SELECT * FROM lpn");
            rsObj = pstmtObj.executeQuery();
            if (rsObj.next()) {
                //("LPN_ID: " + rsObj.getString("LPN_ID"));
            }
            //("\n=====Releasing Connection Object To Pool=====\n");            
        } catch(Exception sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                // Closing ResultSet Object
                if(rsObj != null) {
                    rsObj.close();
                }
                // Closing PreparedStatement Object
                if(pstmtObj != null) {
                    pstmtObj.close();
                }
                // Closing Connection Object
                if(connObj != null) {
                    connObj.close();
                }
            } catch(Exception sqlException) {
                sqlException.printStackTrace();
            }
        }
        jdbcObj.printDbStatus();
    }
}
