package com.manh.dbtransferring.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;





public class OracleConnectionPoolTest {

	final static Logger logger = Logger.getLogger(OracleConnectionPool.class);
	
	private static Connection connection;
	private InputStream input;

	private PropertiesReader prop;
	
	static boolean as400DBPropLogged = false;
	static boolean oracleDBPropLogged = false;

	public static void main(String args[]){// getConnection(){
		BasicDataSource source = new BasicDataSource();
		source.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		source.setUsername("HBAY2016WMSTEST");
		source.setPassword("HBAY2016WMSTEST");
		source.setUrl("jdbc:oracle:thin:@rockwell.us.manh.com:1523:CSO12R1A");
		try {
			connection =  source.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		(connection);
	}
	
	
//	Map<String, String> getConnectionDetails(Map<String, String> dbProperties) {
//		Map<String, String> connDetailsMap = new LinkedHashMap<String, String>();
//		// String osType = "";
//		try {
//			input = PropertiesReader.input;
//			// load a properties file
//			prop.load(input);
//
//			// ("****** dbPropUrl:" + dbPropUrl);
//			// (prop.getProperty(dbPropUrl));
//			// (prop.getProperty(dbProperties.get("userName")));
//			// (prop.getProperty(dbProperties.get("password")));
//			// Populating db properties
//			if (!as400DBPropLogged && dbProperties.get("url").equals("as400_dbUrl")) {
//				logger.info(":: AS400 DB Properties ::");
//			}
//
//			if (!oracleDBPropLogged && dbProperties.get("url").equals("oracle_dbUrl")) {
//				logger.info(":: Oracle DB Properties ::");
//			}
//
//			// logger.info("::DB Properties ::");
//			for (Map.Entry<String, String> entry : dbProperties.entrySet()) {
//				String key = entry.getKey();
//				// ("entry.getValue() :" + entry.getValue());
//				String value = prop.getProperty(entry.getValue());
//				// logger.info("Key:" + key + " Value:" + value);
//
//				if (!as400DBPropLogged && dbProperties.get("url").equals("as400_dbUrl")) {
//					logger.info("Key:" + key + " Value:" + value);
//				}
//				if (!oracleDBPropLogged && dbProperties.get("url").equals("oracle_dbUrl")) {
//					logger.info("Key:" + key + " Value:" + value);
//				}
//
//				connDetailsMap.put(key, value.trim());
//			}
//		} catch (IOException ex) {
//			ExceptionData.exception = ex;
//			logger.error("Exception: ", ex);
//		}
//		return connDetailsMap;
//	}
//
//	// For Pool
//	public Connection getAS400ConnectionForPool() {
//		Map<String, String> dbProperties = new LinkedHashMap<String, String>();
//		Connection as400Connection = null;
//
//		// ("In getAS400Connection()");
//		dbProperties.put("url", "as400_dbUrl");
//		dbProperties.put("userName", "as400_dbUser");
//		dbProperties.put("password", "as400_dbPassword");
//		if (as400PropertiesMap == null)
//			as400PropertiesMap = new DBUtil().getConnectionDetails(dbProperties);
//		try {
//			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
//			as400Connection = DriverManager.getConnection(as400PropertiesMap.get("url"), as400PropertiesMap.get("userName"), as400PropertiesMap.get("password"));
//			// (connection);
//		} catch (Exception e) {
//			ExceptionData.exception = e;
//			logger.error("Exception: ", e);
//		}
//		as400DBPropLogged = true;
//		return as400Connection;
//	}
//
//	public Connection getOracleConnectionForPool() {
//		// To connnect to Local XE use the below configuration
//		Map<String, String> dbProperties = new LinkedHashMap<String, String>();
//		Connection oracleConnection = null;
//
//		// Connection
//
//		try
//		{
//
//			PropertiesHelper propertiesHelper = new PropertiesHelper();
//			String dbType = propertiesHelper.getProperty("dbType");
//
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//
//			if ("OracleExpress".equals(dbType))
//			{
//				String url = propertiesHelper.getProperty("oracleExpress_dbUrl");
//				oracleConnection = DriverManager.getConnection(url);
//			}
//
//			else
//			{
//				dbProperties.put("url", "oracle_dbUrl");
//				dbProperties.put("userName", "oracle_dbUserName");
//				dbProperties.put("password", "oracle_dbPassword");
//				if (oraclePropertiesMap == null)
//					oraclePropertiesMap = new DBUtil().getConnectionDetails(dbProperties);
//				oracleConnection = DriverManager.getConnection(oraclePropertiesMap.get("url"), oraclePropertiesMap.get("userName"), oraclePropertiesMap.get("password"));
//			}
//
//		} catch (Exception e) {
//			ExceptionData.exception = e;
//			logger.error("Exception: ", e);
//		}
//		oracleDBPropLogged = true;
//		return oracleConnection;
//	}
	
	
	
}
