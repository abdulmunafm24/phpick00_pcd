package com.manh.dbtransferring.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import com.manh.dbtransferring.data.DBPropertiesData;

public class TableUtil {
	final static Logger logger = Logger.getLogger(TableUtil.class);

	static boolean as400SelectSqlLogged = false;

	public long getNoOfRowsInTable(String tableName, Connection connection) {
		long noOfRows = -1;
		ResultSet rs = null;
		try {
			rs = connection.prepareStatement("SELECT count(*) FROM " + tableName).executeQuery();
			if (rs.next()) {
				noOfRows = rs.getLong(1);
			}

			return noOfRows;
		} catch (Exception e) {
			logger.error("Exception: ", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (connection != null)
					connection.close();
				// Thread.sleep(1000);
			} catch (Exception e) {
				logger.error("Exception: ", e);
			}
		}
		return -1;
	}

	//
	public StringBuffer removeLastComma(StringBuffer givenString) {
		StringBuffer sbr = new StringBuffer(givenString);
		sbr.deleteCharAt(givenString.length() - 1);
		//givenString = sbr.toString();
		return sbr;
	}

	/**
	 * This Method does not close Connection. Please Take care
	 * @param connection
	 * @param startingRange
	 * @param endingRanges
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public ResultSet getRecordsFromAS400ForTheGivenRange(Connection connection, long startingRange, long endingRanges) throws SQLException, IOException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		//logger.info(Thread.currentThread().getName() + " DBPropertiesData.selectFromAs400SQLString:" + DBPropertiesData.selectFromAs400SQLString);
		logger.info(Thread.currentThread().getName() + " Fetching Rows Between [" + startingRange + "," + endingRanges + "]");
		//logger.info("DBPropertiesData.selectFromAs400SQLString: "+DBPropertiesData.selectFromAs400SQLString);
		preparedStatement = connection.prepareStatement(DBPropertiesData.selectFromAs400SQLString);
		preparedStatement.setLong(1, startingRange);
		preparedStatement.setLong(2, endingRanges);
		resultSet = preparedStatement.executeQuery();
		return resultSet;
	}

}
