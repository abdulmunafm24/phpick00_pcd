package com.manh.dbtransferring.proputil;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.manh.dbtransferring.data.DBPropertiesData;
import com.manh.dbtransferring.util.OSValidator;
import com.manh.dbtransferring.util.PropertiesReader;

public class LoadProperties {

	final static Logger logger = Logger.getLogger(LoadProperties.class);

	public static void createDBPropFilePath_LogFilePath() {
		try {
			if (OSValidator.isWindows()) {
				DBPropertiesData.osType = "WINDOWS";
				DBPropertiesData.input = new FileInputStream(DBPropertiesData.currentDir + "\\db.properties");
				//DBPropertiesData.dbPropertiesPath = DBPropertiesData.currentDir + "\\db.properties";
				DBPropertiesData.logFilePath = DBPropertiesData.currentDir + "\\";

			}

			else {// if (OSValidator.isUnix())
				DBPropertiesData.osType = "NOT Windows";
				DBPropertiesData.input = new FileInputStream(DBPropertiesData.currentDir + "/db.properties");
//				DBPropertiesData.dbPropertiesPath = DBPropertiesData.currentDir + "/db.properties";
				DBPropertiesData.logFilePath = DBPropertiesData.currentDir + "/";
			}
		} catch (Exception e) {
			logger.error(e);
		}
		logger.info("currentDir : " + DBPropertiesData.currentDir);
		logger.info("OS Type:" + DBPropertiesData.osType);
		//logger.info(" dbPropertiesPath:" + DBPropertiesData.dbPropertiesPath);
		logger.info(" logFilePathNName:" + DBPropertiesData.logFilePath);

	}

	public static void loadAllProperties() {
		try {
			populatePropertiesKeys();
			DBPropertiesData.prop.load(DBPropertiesData.input);

			logger.info("Loading Properties");
			for (Map.Entry<String, String> entry : DBPropertiesData.propertiesMap.entrySet()) {
				String key = entry.getKey();
				//String value = DBPropertiesData.prop.getProperty(entry.getValue());
				String value = DBPropertiesData.prop.getProperty(key);
				logger.info("Key:" + key + " Value:" + value);
				DBPropertiesData.propertiesMap.put(key, value.trim());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}

	}

	private static void populatePropertiesKeys() {
		// AS400 Details
		DBPropertiesData.propertiesMap.put("as400_dbUrl", "");
		DBPropertiesData.propertiesMap.put("as400_dbUser", "");
		DBPropertiesData.propertiesMap.put("as400_dbPassword", "");
		DBPropertiesData.propertiesMap.put("as400_schemaName", "");
		// Oracle Details
		DBPropertiesData.propertiesMap.put("oracle_dbUrl", "");
		DBPropertiesData.propertiesMap.put("oracle_dbUserName", "");
		DBPropertiesData.propertiesMap.put("oracle_dbPassword", "");
		//Thread Details
		DBPropertiesData.propertiesMap.put("maxNoOfRecordsPerThread", "");
		DBPropertiesData.propertiesMap.put("maxNoOfThreads", "");

	}
}